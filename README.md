# OpenML dataset: Tesla-Stock-Price

https://www.openml.org/d/43755

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
The subject matter of this dataset explores Tesla's stock price from its initial public offering (IPO) to yesterday.
Content
Within the dataset one will encounter the following:

The date - "Date"
The opening price of the stock - "Open"
The high price of that day - "High"
The low price of that day - "Low"
The closed price of that day - "Close"
The amount of stocks traded during that day - "Volume"
The stock's closing price that has been amended to include any distributions/corporate actions  that occurs before next days open - "Adj[usted] Close"

Acknowledgements
Through Python programming and checking Sentdex out, I acquired the data from Yahoo Finance. The time period represented starts from 06/29/2010 to 03/17/2017.
Inspiration
What happens when the volume of this stock trading increases/decreases in a short and long period of time? What happens when there is a discrepancy between the adjusted close and the next day's opening price?

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43755) of an [OpenML dataset](https://www.openml.org/d/43755). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43755/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43755/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43755/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

